#include <iostream>
#include "Suma.h"

using namespace std;


Suma::Suma(){}

int Suma::sumar_cuadrados(int *arreglo, int n){

    int suma = 0;
    for(int i=0; i<n; i++){
        suma += arreglo[i] * arreglo[i];
    }

    return suma;
}
