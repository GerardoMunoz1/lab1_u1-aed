#include <iostream>
#include "Suma.h"

using namespace std;


int main(){

    string n;
    Suma suma = Suma();

    cout << "¿Cuántos números desea ingresar?: ";
    getline(cin, n);

    int arreglo[stoi(n)];

    for (int i=0; i<stoi(n); i++){

        cout << "Ingrese número para ingresar en la posición " << (i+1) << " del arreglo: ";
        cin >> arreglo[i];
    }

    cout << "\n\t\t = RESULTADO =" << endl;
    cout << "\nLa suma del cuadrado de todos sus números es: " << suma.sumar_cuadrados(arreglo, stoi(n)) << endl;
    cout << "\n¡Hasta pronto!" << endl;

    return 0;
}
