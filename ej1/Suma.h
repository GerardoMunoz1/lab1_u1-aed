#include <iostream>
using namespace std;

#ifndef SUMA_H
#define SUMA_H

class Suma{

    public:
        Suma();
        int sumar_cuadrados(int *arreglo, int n);
};
#endif
