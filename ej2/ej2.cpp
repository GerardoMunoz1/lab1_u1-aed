#include <iostream>
#include "Escribir.h"

using namespace std;


void analizar_frase(string *frases, int n){

    Escribir write = Escribir();

    for(int i=0; i<n; i++){
        cout << "\n>> La frase [" << frases[i] << "] tiene en su contenido: " << endl;
        cout << "\n[1] > Un total de " << write.leer_mayusculas(frases[i]) << " minúsculas." << endl;
        cout << "[2] > Un total de " << write.leer_minusculas(frases[i]) << " mayúsculas." << endl << endl;
    }
}

int main(){

    string n;

    cout << "¿Cuántas palabras desea calcular?: ";
    getline(cin, n);
    string frases[stoi(n)];

    for(int i=0; i<stoi(n); i++){
        cout << "Ingrese la frase " << (i+1) << ": ";
        getline(cin, frases[i]);
    }
    analizar_frase(frases, stoi(n));

    return 0;
}
