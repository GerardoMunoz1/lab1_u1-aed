#include <iostream>
#include <ctype.h>
#include "Escribir.h"

using namespace std;

Escribir::Escribir(){}

int Escribir::leer_mayusculas(string frase){

    this->mayusculas = 0;
    for(int i=0; i<frase.size(); i++){
        if(isupper(frase[i])){
            this->mayusculas++;
        }
    }

    return this->mayusculas;
}


int Escribir::leer_minusculas(string frase){

    this->minusculas = 0;
    for(int i=0; i<frase.size(); i++){
        if(islower(frase[i])){
            this->minusculas++;
        }
    }

    return this->minusculas;
}
