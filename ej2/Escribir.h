#include <iostream>

using namespace std;

#ifndef ESCRIBIR_H
#define ESCRIBIR_H

class Escribir{

    private:
        int mayusculas;
        int minusculas;

    public:
        Escribir();
        int leer_minusculas(string frase);
        int leer_mayusculas(string frase);
        
};
#endif
